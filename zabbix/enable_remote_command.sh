#!/bin/bash
# self executing script: wget -O - "https://gitlab.com/dvdred/agnostic-dump/raw/master/zabbix/enable_remote_command.sh" | bash

yum install -y crudini || yum install -y "http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/c/crudini-0.9-1.el7.noarch.rpm"
yum install -y policycoreutils-python
crudini --set /etc/zabbix/zabbix_agentd.conf "" EnableRemoteCommands 1
wget "https://gitlab.com/dvdred/agnostic-dump/raw/master/zabbix/selinux_c7_module_zabbix_agent.pp" -O /tmp/zabbix_agent.pp
semodule -i /tmp/zabbix_agent.pp
systemctl restart zabbix-agent
